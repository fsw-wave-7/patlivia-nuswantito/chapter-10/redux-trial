import React, {Component} from "react";
import {Row, Col, Container} from "react-bootstrap";
import {connect} from "react-redux";
import Person from "./Person";
import PersonAPI from "../assets/person";
import * as action from "../redux/actions/person";

class PersonContainer extends Component {
	componentDidMount() {
		// PersonAPI.all().then((persons) => {
		// 	this.props.dispatch({
		// 		type: "person/INIT",
		// 		payload: persons,
		// 	});
		// });
		this.props.getAllPerson();
	}

	createCard = (personProps) => {
		<Col xs={4}>
			<Person {...personProps}></Person>
		</Col>;
	};

	createRow = (rows) => {
		<Row key={`${Math.random()}-${Date.now()}`}>
			{rows.map((i) => {
				this.createCard(Object.assign(i, {key: i.id}));
			})}
		</Row>;
	};

	render() {
		// PROPS DARI CONNECT FUNCTION REACT-REDUX
		const {data} = this.props;
		const contents = [];
		// for (let i = 0; i < data.length; i += 3) {
		// 	contents.push(data.slice(i, i + 3));
		// }

		data.map((e) => {
			contents.push(e);
		});
		console.log(contents);
		return (
			<Container fluid className="p-4">
				{/* {contents.map((i) => this.createRow(i))} */}
				{this.createRow(contents)}
			</Container>
		);
	}
}

const mapStateToProps = (state) => ({
	// STATE DARI STORE
	data: state.persons,
});

const mapDispatchToProps = {
	getAllPerson: action.getAllPerson,
	addPerson: action.addPerson,
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonContainer);
